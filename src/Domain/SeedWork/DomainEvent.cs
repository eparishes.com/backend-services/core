using System;
using MediatR;

namespace Domain.SeedWork
{
    public abstract class DomainEvent : IDomainEvent, INotification
    {
        protected DomainEvent()
        {
            OccuredOn = DateTime.UtcNow;
        }

        public DateTime OccuredOn { get; }
    }
}