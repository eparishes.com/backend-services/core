using System;
using System.Collections.Generic;
using Domain.SeedWork;

namespace Domain.User
{
    public class User : IAggregateRoot
    {
        public User(string firstName, string lastName)
        {
            FirstName = firstName ?? throw new ArgumentNullException(nameof(firstName));
            LastName = lastName ?? throw new ArgumentNullException(nameof(lastName));
        }

        protected User()
        {
        }

        public string FirstName { get; }
        public string LastName { get; }
        public List<Guid> Roles { get; } = new List<Guid>();

        public void AddRole(Guid roleId)
        {
            Roles.Add(roleId);
        }
    }
}