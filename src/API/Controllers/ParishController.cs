﻿using System;
using System.Net;
using API.Dto.Parish;
using Domain.Parish;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace API.Controllers
{
    [ApiController]
    [Route("parishes")]
    public class ParishController : ControllerBase
    {
        private readonly ILogger<ParishController> _logger;

        public ParishController(ILogger<ParishController> logger)
        {
            _logger = logger;
        }

        [HttpGet("{parishId}")]
        [ProducesResponseType((int) HttpStatusCode.Created)]
        [ProducesResponseType((int) HttpStatusCode.BadRequest)]
        [ProducesResponseType((int) HttpStatusCode.NotFound)]
        public IActionResult GetParish([FromRoute] string parishId)
        {
            try
            {
                var parsedParishId = Guid.Parse(parishId);
            }
            catch (FormatException ex)
            {
                return BadRequest(ex.Message);
            }

            throw new NotImplementedException();
        }

        [HttpPost]
        [ProducesResponseType((int) HttpStatusCode.Created)]
        [ProducesResponseType((int) HttpStatusCode.BadRequest)]
        public Parish CreateParish(CreateParishDto data)
        {
            _logger.LogTrace("Received request for Parish creation", data);
            throw new NotImplementedException();
        }

        [HttpPut]
        [ProducesResponseType((int) HttpStatusCode.OK)]
        [ProducesResponseType((int) HttpStatusCode.BadRequest)]
        public Parish UpdateParish(UpdateParishDto data)
        {
            throw new NotImplementedException();
        }

        [HttpDelete]
        [ProducesResponseType((int) HttpStatusCode.OK)]
        [ProducesResponseType((int) HttpStatusCode.BadRequest)]
        [ProducesResponseType((int) HttpStatusCode.Unauthorized)]
        public void RemoveParish(string parishId)
        {
            throw new NotImplementedException();
        }
    }
}