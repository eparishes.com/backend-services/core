using Domain.SeedWork;

namespace Domain.Role
{
    public class Role : Entity, IAggregateRoot
    {
        private Role()
        {
        } // EF

        public Role(RoleType roleType, string roleName)
        {
            RoleType = roleType;
            RoleName = roleName;
        }

        public RoleType RoleType { get; }
        public string RoleName { get; }
    }
}