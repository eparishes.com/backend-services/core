namespace Domain.SeedWork
{
    public interface IBaseRepository
    {
        IUnitOfWork UnitOfWork { get; }
    }
}