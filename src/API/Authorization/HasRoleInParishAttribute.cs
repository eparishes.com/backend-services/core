using System;
using Domain.SeedWork;
using Microsoft.AspNetCore.Authorization;

namespace API.Authorization
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = true)]
    internal class HasRoleInParishAttribute : AuthorizeAttribute
    {
        internal const string HasRoleInParishPolicyName = "HasPermission";

        public HasRoleInParishAttribute(string name, Type aggregateRoot) : base(HasRoleInParishPolicyName)
        {
            if (!typeof(IAggregateRoot).IsAssignableFrom(aggregateRoot))
                throw new ArgumentException(
                    "HasRoleInParishAttribute can be used only with entities implementing IAggregateRoot");

            Aggregate = aggregateRoot;
            Name = name;
        }

        public string Name { get; }
        public Type Aggregate { get; }
    }
}