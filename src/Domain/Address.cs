using System;

namespace Domain
{
    public class Address : IEquatable<Address>, ICloneable
    {
        protected Address()
        {
        }

        public Address(string street, string streetDetails, string city, string postalCode, string country)
        {
            Street = street;
            StreetDetails = streetDetails;
            City = city;
            PostalCode = postalCode;
            Country = country;
        }

        public string Street { get; }
        public string StreetDetails { get; }
        public string City { get; private set; }
        public string PostalCode { get; }
        public string Country { get; }

        public object Clone()
        {
            return MemberwiseClone();
        }

        public bool Equals(Address other)
        {
            return other.City == City && other.Country == Country && other.Street == Street &&
                   other.PostalCode == PostalCode && other.StreetDetails == StreetDetails;
        }

        public void UpdateCity(string city)
        {
            City = city;
        }
    }
}