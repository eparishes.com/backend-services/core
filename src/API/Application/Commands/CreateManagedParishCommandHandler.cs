using System;
using System.Threading;
using System.Threading.Tasks;
using Domain;
using Domain.Interfaces;
using Domain.Parish;
using MediatR;
using Microsoft.Extensions.Logging;

namespace API.Application.Commands
{
    public class CreateManagedParishCommandHandler : IRequestHandler<CreateManagedParishCommand, bool>
    {
        private readonly ILogger<CreateManagedParishCommandHandler> _logger;
        private readonly IMediator _mediator;
        private readonly IMembershipService _membershipService;
        private readonly IParishRepository _parishRepository;

        public CreateManagedParishCommandHandler(IParishRepository parishRepository, IMediator mediator,
            ILogger<CreateManagedParishCommandHandler> logger, IMembershipService membershipService)
        {
            _parishRepository = parishRepository ?? throw new ArgumentNullException(nameof(parishRepository));
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _membershipService = membershipService ?? throw new ArgumentNullException(nameof(membershipService));
        }

        public async Task<bool> Handle(CreateManagedParishCommand request, CancellationToken cancellationToken)
        {
            var address = new Address(request.Street, request.StreetDetails, request.City, request.PostalCode,
                request.Country);
            var parish = new Parish(request.Invocation, address);
            _logger.LogInformation("Creating Parish: {@Parish}", parish);
            _parishRepository.Add(parish);

            // User creating the parish is suppose to be assigned as the RolesInParish.Admin
            await _membershipService.AssignUserAsParishAdmin(request.UserId, parish.Id); // TODO move to event handler

            return await _parishRepository.UnitOfWork.SaveEntitiesAsync(cancellationToken);
        }
    }
}