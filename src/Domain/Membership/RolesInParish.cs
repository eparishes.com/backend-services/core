namespace Domain.Membership
{
    public enum RoleInParish
    {
        Admin,
        Blogger,
        Photographer,
        Priest
    }
}