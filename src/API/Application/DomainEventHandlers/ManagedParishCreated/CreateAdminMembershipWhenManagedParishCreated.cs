using System;
using System.Threading;
using System.Threading.Tasks;
using Domain.Parish.Events;
using MediatR;

namespace API.Application.DomainEventHandlers.ManagedParishCreated
{
    public class CreateAdminMembershipWhenManagedParishCreated : INotificationHandler<ManagedParishCreatedDomainEvent>
    {
        public Task Handle(ManagedParishCreatedDomainEvent notification, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }
    }
}