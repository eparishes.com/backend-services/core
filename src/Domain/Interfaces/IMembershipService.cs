using System;
using System.Threading.Tasks;

namespace Domain.Interfaces
{
    public interface IMembershipService
    {
        Task AssignUserAsParishAdmin(string userId, Guid parishId);
        Task AddRoleToUser(string userId, Guid parishId, Guid roleId);
    }
}