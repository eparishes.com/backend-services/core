using System;
using System.Collections.ObjectModel;
using System.Linq;
using Domain.Exceptions;
using Domain.SeedWork;
using NetTopologySuite.Geometries;
using NodaTime;

namespace Domain.ChurchAggregate
{
    public class Church : Entity, IAggregateRoot, IParishOwnedEntity
    {
        protected Church()
        {
        }

        public Church(Guid id, string invocation, Point location, Guid parishId, Address address)
        {
            if (id == Guid.Empty) throw new ArgumentException("Id has to be defined.");
            if (invocation.Length == 0) throw new ArgumentException("Invocation of the church cannot be empty.");
            if (parishId == Guid.Empty) throw new ArgumentException("ParishId has to be defined.");
            Invocation = invocation;
            Location = location;
            Masses = new Collection<Mass>();
            Address = address;
        }

        public Point Location { get; }
        public string Invocation { get; }
        public Collection<Mass> Masses { get; }
        public Address Address { get; }
        public Guid ParishId { get; }

        public void AddMass(Mass mass)
        {
            if (Masses.Any(m => m.Id == mass.Id)) throw new DomainValidationException("Duplicated mass.");

            if (!IsTimeOffsetSatisfied(mass)) CreateNotSatisfiedTimeOffsetResult();
            Masses.Add(mass);
        }

        public void UpdateMassTime(Guid massId, LocalTime time)
        {
            if (Masses.Any(m => massId == m.Id))
                throw new DomainValidationException("Mass with given id does not exist");

            var mass = Masses.First(m => m.Id == massId);
            mass.UpdateTime(time);
            if (!IsTimeOffsetSatisfied(mass)) CreateNotSatisfiedTimeOffsetResult();
        }

        private bool IsTimeOffsetSatisfied(Mass massBeingChecked)
        {
            foreach (var mass in Masses)
                if ((massBeingChecked.Time - mass.Time).Minutes < 30)
                    return false;

            return true;
        }

        private static void CreateNotSatisfiedTimeOffsetResult()
        {
            throw new DomainValidationException("Time difference between masses is below required offset.");
        }
    }
}