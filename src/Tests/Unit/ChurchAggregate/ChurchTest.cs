using System;
using Domain;
using Domain.ChurchAggregate;
using Domain.Exceptions;
using NetTopologySuite.Geometries;
using NodaTime;
using Xunit;

namespace Tests.Unit.ChurchAggregate
{
    public class ChurchTest
    {
        [Fact]
        public void AddsMassOnlyOnce()
        {
            var church = new Church(Guid.NewGuid(), "Saint Peter", Point.Empty, Guid.NewGuid(),
                new Address("", "", "", "", ""));
            var mass = new Mass(Guid.NewGuid(), LocalTime.Noon, "Sum", "It's a mass.");
            church.AddMass(mass);
            var ex = Assert.Throws<DomainValidationException>(() => church.AddMass(mass));
            Assert.Equal("Duplicated mass.", ex.Message);
        }

        [Fact]
        public void AllowsToCreateChurch()
        {
            const string invocation = "Saint Peter";
            var church = new Church(Guid.NewGuid(), invocation, new Point(0, 0), Guid.NewGuid(),
                new Address("Street", "22", "Wroclaw", "51-200", "Poland"));
            Assert.Equal(church.Invocation, invocation);
            Assert.Equal(new Address("Street", "22", "Wroclaw", "51-200", "Poland"), church.Address);
            Assert.Equal(new Point(0, 0), church.Location);
        }

        [Fact]
        public void DoesNotAllowToAddMassesWithOffsetBelowRequiredValue()
        {
            var church = new Church(Guid.NewGuid(), "Saint Peter", Point.Empty, Guid.NewGuid(),
                new Address("", "", "", "", ""));
            var firstMass = new Mass(Guid.NewGuid(), LocalTime.Noon, "Sum", "It's a mass.");
            church.AddMass(firstMass);
            var ex = Assert.Throws<DomainValidationException>(() => church.AddMass(new Mass(Guid.NewGuid(),
                LocalTime.Noon.Plus(Period.FromMinutes(20)), "After sum",
                "It's a mass.")));

            Assert.Equal("Time difference between masses is below required offset.", ex.Message);
        }

        [Fact]
        public void DoesNotAllowToCreateWithEmptyDedication()
        {
            Assert.Throws<ArgumentException>(() =>
                new Church(Guid.NewGuid(), "", new Point(0, 0), Guid.NewGuid(), new Address("", "", "", "", "")));
        }
    }
}