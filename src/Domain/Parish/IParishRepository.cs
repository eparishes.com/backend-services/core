using Domain.SeedWork;

namespace Domain.Parish
{
    public interface IParishRepository : IBaseRepository
    {
        Parish Add(Parish parish);
    }
}