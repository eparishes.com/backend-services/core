using System;
using System.Threading;
using System.Threading.Tasks;
using Domain.ChurchAggregate;
using Domain.Parish;
using Domain.SeedWork;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure
{
    public class CoreContext : DbContext, IUnitOfWork
    {
        public DbSet<Church> Churches { get; set; }
        public DbSet<Parish> Parishes { get; set; }

        public Task<bool> SaveEntitiesAsync(CancellationToken cancellationToken = default)
        {
            throw new NotImplementedException();
        }
    }
}