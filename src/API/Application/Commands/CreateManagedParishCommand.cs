using System.Runtime.Serialization;

namespace API.Application.Commands
{
    [DataContract]
    public class CreateManagedParishCommand : BaseCreateParishCommand
    {
        public CreateManagedParishCommand(string invocation, string street, string streetDetails, string city,
            string postalCode,
            string country, string userId) : base(invocation, street, streetDetails, city, postalCode,
            country)
        {
            UserId = userId;
        }

        [DataMember] public string UserId { get; private set; }
    }
}