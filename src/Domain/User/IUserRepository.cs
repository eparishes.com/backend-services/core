using System.Threading.Tasks;

namespace Domain.User
{
    public interface IUserRepository
    {
        Task AddAsync(User user);
        Task Update(User user);
    }
}