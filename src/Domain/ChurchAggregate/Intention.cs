using Domain.SeedWork;
using NodaTime;

namespace Domain.ChurchAggregate
{
    public class Intention : Entity
    {
        protected Intention()
        {
        } // for EF

        public Intention(string title, LocalDate date)
        {
            Title = title;
            Date = date;
        }

        public string Title { get; }
        public LocalDate Date { get; }
    }
}