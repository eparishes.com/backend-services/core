[![pipeline status](https://gitlab.com/eparishes.com/core/badges/master/pipeline.svg)](https://gitlab.com/eparishes.com/core/-/commits/master)
[![coverage report](https://gitlab.com/eparishes.com/core/badges/master/coverage.svg)](https://gitlab.com/eparishes.com/core/-/commits/master)

# eParishes.com
## Core backend (WIP, MVP not reached yet)

This is the repo for the main backend service for eParishes.com.
It is subject for change if in the future the developers will agree that other structure is more appropriate.