using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace API.Controllers
{
    public abstract class BaseApiController<T> : ControllerBase
    {
        private readonly ILogger<T> _logger;
        private readonly IMediator _mediator;


        protected BaseApiController(IMediator mediator, ILogger<T> logger)
        {
            _mediator = mediator;
            _logger = logger;
        }


        // protected new IActionResult Response(Result result)
        // {
        //     if (result.IsFailure)
        //     {
        //         return result.Reason?.Reason switch
        //         {
        //             Result.FailureResult.FailureReason.Authorization => Forbid(result.Reason?.Message),
        //             Result.FailureResult.FailureReason.Validation => BadRequest(result.Reason?.Message),
        //             _ => this.Problem("Not handled failure reason")
        //         };
        //     }
        //
        //     return result.Result;
        // }
    }
}