using System;
using Domain.ChurchAggregate;
using NodaTime;
using NodaTime.Calendars;
using Xunit;

namespace Tests.Unit.ChurchAggregate
{
    public class MassTest
    {
        [Fact]
        public void ProperlyCreatesOneOffMass()
        {
            var mass = new Mass(Guid.NewGuid(), new LocalTime(12, 00, 00), new LocalDate(Era.Common, 2020, 11, 11),
                new LocalDate(Era.Common, 2020, 11, 11));
            Assert.Equal(Mass.MassType.OneOff, mass.Type);
        }

        [Fact]
        public void ProperlyCreatesPermanentMass()
        {
            var mass = new Mass(Guid.NewGuid(), new LocalTime(12, 00, 00));
            Assert.Equal(LocalDate.MinIsoValue, mass.StartDate);
            Assert.Equal(LocalDate.MaxIsoValue, mass.EndDate);
            Assert.Equal(Mass.MassType.Permanent, mass.Type);
            Assert.Equal("", mass.Description);
            Assert.Equal("", mass.Title);
        }

        [Fact]
        public void ProperlyCreatesTemporaryMass()
        {
            var mass = new Mass(Guid.NewGuid(), new LocalTime(12, 00, 00), new LocalDate(Era.Common, 2020, 11, 11),
                new LocalDate(Era.Common, 2020, 12, 20), "Title", "Description");
            Assert.Equal(Mass.MassType.Temporary, mass.Type);
            Assert.Equal("Description", mass.Description);
            Assert.Equal("Title", mass.Title);
        }
    }
}