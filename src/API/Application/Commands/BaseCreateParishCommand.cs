using System.Runtime.Serialization;
using MediatR;

namespace API.Application.Commands
{
    // This is ground work for future feature of socially managed parishes, to be done in v2
    [DataContract]
    public class BaseCreateParishCommand : IRequest<bool>
    {
        public BaseCreateParishCommand(string invocation, string street, string streetDetails, string city,
            string postalCode,
            string country)
        {
            Invocation = invocation;
            Street = street;
            StreetDetails = streetDetails;
            City = city;
            PostalCode = postalCode;
            Country = country;
        }

        [DataMember] public string Invocation { get; private set; }
        [DataMember] public string Street { get; private set; }
        [DataMember] public string StreetDetails { get; private set; }
        [DataMember] public string City { get; private set; }
        [DataMember] public string PostalCode { get; private set; }
        [DataMember] public string Country { get; private set; }
    }
}