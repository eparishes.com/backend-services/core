using System;
using System.Runtime.Serialization;

namespace Domain.Exceptions
{
    public class DomainAuthorizationException : DomainException
    {
        public DomainAuthorizationException()
        {
        }

        protected DomainAuthorizationException(SerializationInfo? info, StreamingContext context) : base(info, context)
        {
        }

        public DomainAuthorizationException(string? message) : base(message)
        {
        }

        public DomainAuthorizationException(string? message, Exception? innerException) : base(message, innerException)
        {
        }
    }
}