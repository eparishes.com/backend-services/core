using System;
using System.Runtime.Serialization;

namespace Domain.Exceptions
{
    public class DomainValidationException : DomainException
    {
        public DomainValidationException()
        {
        }

        protected DomainValidationException(SerializationInfo? info, StreamingContext context) : base(info, context)
        {
        }

        public DomainValidationException(string? message) : base(message)
        {
        }

        public DomainValidationException(string? message, Exception? innerException) : base(message, innerException)
        {
        }
    }
}