using System.Threading.Tasks;

namespace Domain.Role
{
    public interface IRoleRepository
    {
        Task<Role> FindByNameAsync(string name);
    }
}