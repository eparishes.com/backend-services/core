using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Domain.SeedWork;

namespace Domain.Membership
{
    public interface IMembershipRepository : IBaseRepository
    {
        Membership Add(Membership membership);
        Task<List<Membership>> GetUserMembershipsWithRolesAsync(Guid userId);
    }
}