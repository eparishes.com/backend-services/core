namespace Domain.Role
{
    public enum RoleType
    {
        Membership,
        Administrative
    }
}