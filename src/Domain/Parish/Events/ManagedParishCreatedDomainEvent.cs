using System;
using Domain.SeedWork;

namespace Domain.Parish.Events
{
    public class ManagedParishCreatedDomainEvent : DomainEvent
    {
        public string UserId { get; private set; }
        public Guid ParishId { get; private set; }
    }
}