using System;
using System.Collections.ObjectModel;
using Domain.Exceptions;
using Domain.SeedWork;

namespace Domain.Parish
{
    public class Parish : Entity, IAggregateRoot
    {
        public static int _minimumInvocationLength = 5;

        protected Parish()
        {
        }

        public Parish(string invocation, Address address)
        {
            if (invocation.Length < _minimumInvocationLength)
                throw new ArgumentException(ParishInvocationLengthErrorMessage());
            Invocation = invocation;
            Address = address;
        }

        public string Invocation { get; private set; }
        public Collection<Guid> Churches { get; } = new Collection<Guid>();
        public Address Address { get; private set; }

        public void UpdateInvocation(string newInvocation)
        {
            if (newInvocation.Length < _minimumInvocationLength)
                throw new DomainValidationException(ParishInvocationLengthErrorMessage());
            Invocation = newInvocation;
        }

        public void UpdateAddress(Address newAddress)
        {
            Address = newAddress;
        }

        public void AddChurch(Guid churchId)
        {
            if (Churches.Contains(churchId))
                throw new DomainValidationException("Church is already assigned to the given parish.");
            Churches.Add(churchId);
        }

        private static string ParishInvocationLengthErrorMessage()
        {
            return $"Parish invocation has to be at least {_minimumInvocationLength} characters long";
        }
    }
}