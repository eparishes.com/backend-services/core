using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;

namespace API.Authorization
{
    internal class HasRoleInParishAuthorizationHandler : AttributeAuthorizationHandler<
        HasRoleInParishAuthorizationRequirement, HasRoleInParishAttribute>
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public HasRoleInParishAuthorizationHandler(IHttpContextAccessor httpContextAccessor)
        {
            // add db context
            _httpContextAccessor = httpContextAccessor;
        }

        protected override async Task HandleRequirementAsync(AuthorizationHandlerContext context,
            HasRoleInParishAuthorizationRequirement requirement, IEnumerable<HasRoleInParishAttribute> attributes)
        {
            var httpCtx = _httpContextAccessor.HttpContext;
            Guid parishId;
            if (!Guid.TryParse(httpCtx.GetRouteData()
                .Values["parishId"]
                ?.ToString(), out parishId))
            {
                context.Fail();
                return;
            }

            // TODO implement
            var pendingPermissionsValidations =
                attributes.Select(attribute => IsUserAllowedToPerformOperation(attribute, parishId))
                    .ToList();

            var permissionsValidationsResults = await Task.WhenAll(pendingPermissionsValidations);
            if (permissionsValidationsResults.All(b => b))
            {
                context.Succeed(requirement);
                return;
            }

            context.Fail();
        }

        private async Task<bool> IsUserAllowedToPerformOperation(HasRoleInParishAttribute hasRoleInParishAttribute,
            Guid parishId)
        {
            return false;
        }
    }
}