using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Domain.Parish;
using Domain.SeedWork;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Repositories
{
    public class ParishRepository : IParishRepository
    {
        private readonly CoreContext _context;

        public ParishRepository(CoreContext context)
        {
            _context = context;
        }

        public IUnitOfWork UnitOfWork => _context;

        public Parish Add(Parish entity)
        {
            return _context.Parishes.Add(entity)
                .Entity;
        }

        public async Task<Parish> FindById(Guid id)
        {
            var parish = await _context.Parishes.Include(p => p.Address)
                             .FirstOrDefaultAsync(p => p.Id == id) ??
                         _context.Parishes.Local.FirstOrDefault(p => p.Id == id);

            return parish;
        }

        public IQueryable<Parish> FindAll()
        {
            throw new NotImplementedException();
        }

        public IQueryable<Parish> FindBy(Expression<Func<Parish, bool>> expression)
        {
            throw new NotImplementedException();
        }

        public Task<Parish> UpdateAsync(Parish entity)
        {
            throw new NotImplementedException();
        }

        public Task<Parish> DeleteAsync(Parish entity)
        {
            throw new NotImplementedException();
        }
    }
}