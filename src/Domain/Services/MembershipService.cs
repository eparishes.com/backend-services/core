using System;
using System.Threading.Tasks;
using Domain.Interfaces;
using Domain.Membership;
using Domain.Role;

namespace Domain.Services
{
    public class MembershipService : IMembershipService
    {
        private readonly IMembershipRepository _membershipRepository;
        private readonly IRoleRepository _roleRepository;

        public async Task AssignUserAsParishAdmin(string userId, Guid parishId)
        {
            var role = await _roleRepository.FindByNameAsync(nameof(RoleInParish.Admin));
            var membership = _membershipRepository.Add(new Membership.Membership(parishId, userId, role.Id));
            await _membershipRepository.UnitOfWork.SaveEntitiesAsync();
        }

        public async Task AddRoleToUser(string userId, Guid parishId, Guid roleId)
        {
            // it will become useful when we will create custom roles in Parishes
            // so we can check here if the role belongs to the parish etc
            throw new NotImplementedException();
        }
    }
}