using System;
using Domain.SeedWork;

namespace Domain.Membership
{
    public class Membership : Entity, IAggregateRoot
    {
        public Membership(Guid parishId, string userId, Guid roleId, bool isActive = true)
        {
            ParishId = parishId;
            UserId = userId;
            RoleId = roleId;
            IsActive = isActive;
        }

        public Guid ParishId { get; }
        public string UserId { get; }
        public Guid RoleId { get; }
        public bool IsActive { get; private set; }

        public void Deactivate()
        {
            IsActive = false;
        }

        public void Activate()
        {
            IsActive = true;
        }
    }
}