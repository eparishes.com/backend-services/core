using System;
using System.Collections.Generic;
using Domain.Exceptions;
using Domain.SeedWork;
using NodaTime;

namespace Domain.ChurchAggregate
{
    public class Mass : Entity
    {
        public enum MassType
        {
            Permanent,
            OneOff,
            Temporary
        }

        protected Mass()
        {
        } // for EF

        public Mass(Guid id, LocalTime time, LocalDate startDate, LocalDate endDate, string title = "",
            string description = "")
        {
            Id = id;
            Time = time;
            Title = title;
            Description = description;
            StartDate = startDate;
            EndDate = endDate;
        }

        // convenience constructor for the permanent mass
        public Mass(Guid id, LocalTime time, string title = "", string description = "")
        {
            if (id == Guid.Empty) throw new ArgumentException("Id has to be defined.");

            Id = id;
            Time = time;
            Title = title;
            Description = description;
            StartDate = LocalDate.MinIsoValue;
            EndDate = LocalDate.MaxIsoValue;
        }

        public LocalTime Time { get; private set; }
        public string? Title { get; }
        public string? Description { get; }
        public LocalDate StartDate { get; private set; }
        public LocalDate EndDate { get; private set; }
        public List<Intention>? Intentions { get; private set; }

        public MassType Type
        {
            get
            {
                if (StartDate.Equals(EndDate)) return MassType.OneOff;

                if (!StartDate.Equals(LocalDate.MinIsoValue) || !EndDate.Equals(LocalDate.MaxIsoValue))
                    return MassType.Temporary;

                return MassType.Permanent;
            }
        }

        public void UpdatePeriod(LocalDate startDate, LocalDate endDate)
        {
            if (startDate > endDate) throw new DomainValidationException("StartDate cannot be later than EndDate");

            StartDate = startDate;
            EndDate = endDate;
        }

        public void UpdateTime(LocalTime time)
        {
            Time = time;
        }

        public void AddIntention(Intention intention)
        {
            if (!(StartDate <= intention.Date && EndDate >= intention.Date))
                throw new DomainValidationException("Intention date is outside of the mass period.");

            if (Intentions?.Find(i => i.Date.Equals(intention.Date)) != null)
                throw new DomainValidationException("Duplicated intention.");

            Intentions?.Add(intention);
        }
    }
}