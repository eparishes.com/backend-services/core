using System;
using Domain.Exceptions;
using Domain.Parish;
using Tests.Utils;
using Xunit;

namespace Tests.Unit.ParishAggregate
{
    public class ParishTest
    {
        [Fact]
        public void AllowsToAddChurchOnlyOnce()
        {
            var church = TestDataGenerator.ChurchTests.GetExampleChurch();
            var parish = TestDataGenerator.ParishTests.GetExampleParish();
            parish.AddChurch(church.Id);
            Assert.Collection(parish.Churches, guid => Assert.Equal(church.Id, guid));
            var ex = Assert.Throws<DomainValidationException>(() =>
                parish.AddChurch(church.Id)
            );
            Assert.Equal("Church is already assigned to the given parish.", ex.Message);
        }

        [Fact]
        public void AllowsToUpdateAddressProperly()
        {
            var address = TestDataGenerator.AddressTests.GetTestAddress();
            var parish = new Parish("Invocation", TestDataGenerator.AddressTests.GetTestAddress());
            parish.UpdateAddress(TestDataGenerator.AddressTests.GetTestAddressAlternative());
            Assert.Equal(TestDataGenerator.AddressTests.GetTestAddressAlternative(), parish.Address);
        }

        [Fact]
        public void DoesNotAllowToCreateParishWithImpossiblyShortInvocation()
        {
            var ex = Assert.Throws<ArgumentException>(() =>
            {
                new Parish("Sho", TestDataGenerator.AddressTests.GetTestAddress());
            });
            Assert.Equal("Parish invocation has to be at least 5 characters long", ex.Message);
        }

        [Fact]
        public void DoesNotAllowToUpdateParishWithImpossiblyShortInvocation()
        {
            var parish = new Parish("Valid Invocation", TestDataGenerator.AddressTests.GetTestAddress());
            var ex = Assert.Throws<DomainValidationException>(() => { parish.UpdateInvocation("Inva"); });
            Assert.Equal("Parish invocation has to be at least 5 characters long", ex.Message);
        }

        [Fact]
        public void ProperlyCreatesParish()
        {
            var parish = new Parish("Example Invocation", TestDataGenerator.AddressTests.GetTestAddress());
            Assert.Equal(TestDataGenerator.AddressTests.GetTestAddress(), parish.Address);
            Assert.Equal("Example Invocation", parish.Invocation);
        }
    }
}