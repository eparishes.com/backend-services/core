using System.ComponentModel.DataAnnotations;
using Domain;

namespace API.Dto.Parish
{
    public abstract class BaseParishDto
    {
        [Required]
        [StringLength(100, ErrorMessage = "{0} length must be between {2} and {1}", MinimumLength = 5)]
        public string Invocation { get; private set; }

        [Required] public Address Address { get; private set; }
    }
}