using Domain.SeedWork;

namespace Domain.Membership
{
    public class MembershipRole : Entity
    {
        public MembershipRole()
        {
        }

        public MembershipRole(RoleInParish name)
        {
            this.name = name;
        }

        public RoleInParish name { get; }
    }
}