using System;
using System.ComponentModel.DataAnnotations;

namespace API.Dto.Parish
{
    public class UpdateParishDto : BaseParishDto
    {
        [Required]
        [RegularExpression("(^([0-9A-Fa-f]{8}[-][0-9A-Fa-f]{4}[-][0-9A-Fa-f]{4}[-][0-9A-Fa-f]{4}[-][0-9A-Fa-f]{12})$)")]
        public Guid Id { get; set; }
    }
}