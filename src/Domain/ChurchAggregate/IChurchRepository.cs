using System.Collections.Generic;
using System.Threading.Tasks;
using Domain.SeedWork;
using NetTopologySuite.Geometries;

namespace Domain.ChurchAggregate
{
    public interface IChurchRepository : IBaseRepository
    {
        public Task<List<Church>> FindChurchesInArea(Point centerPoint, Polygon area);
        public Task<List<Church>> FindNearbyChurches(Point centerPoint, int radius);
    }
}