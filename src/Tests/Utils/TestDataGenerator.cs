using System;
using Domain;
using Domain.ChurchAggregate;
using Domain.Parish;
using NetTopologySuite.Geometries;

namespace Tests.Utils
{
    public static class TestDataGenerator
    {
        public static class AddressTests
        {
            public static Address GetTestAddress()
            {
                return new Address("Street", "22", "Wroclaw", "51-200", "Poland");
            }

            public static Address GetTestAddressAlternative()
            {
                return new Address("New Street", "25", "Warsaw", "51-201", "Poland");
            }
        }

        public static class ChurchTests
        {
            public static Church GetExampleChurch()
            {
                return new Church(Guid.NewGuid(), "Some Invocation", new Point(0, 0), Guid.NewGuid(),
                    new Address("Street", "22", "Wroclaw", "51-200", "Poland"));
            }
        }

        public static class ParishTests
        {
            public static Parish GetExampleParish()
            {
                return new Parish("Valid Invocation", AddressTests.GetTestAddress());
            }
        }
    }
}