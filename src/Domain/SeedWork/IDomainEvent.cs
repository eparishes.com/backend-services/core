using System;

namespace Domain.SeedWork
{
    public interface IDomainEvent
    {
        DateTime OccuredOn { get; }
    }
}