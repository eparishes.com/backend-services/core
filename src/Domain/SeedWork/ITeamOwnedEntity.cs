using System;

namespace Domain.SeedWork
{
    public interface IParishOwnedEntity
    {
        public Guid ParishId { get; }
    }
}