using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Domain.ChurchAggregate;
using Domain.SeedWork;
using Microsoft.EntityFrameworkCore;
using NetTopologySuite.Geometries;

namespace Infrastructure.Repositories
{
    public class ChurchRepository : IChurchRepository
    {
        private readonly CoreContext _context;

        public ChurchRepository(CoreContext context)
        {
            _context = context;
        }

        public IUnitOfWork UnitOfWork => _context;

        public Task<List<Church>> FindChurchesInArea(Point centerPoint, Polygon area)
        {
            return _context.Churches.Where(c => area.Contains(centerPoint))
                .ToListAsync();
        }

        public Task<List<Church>> FindNearbyChurches(Point centerPoint, int radius)
        {
            return _context.Churches.Where(c => c.Location.Distance(centerPoint) <= radius)
                .ToListAsync();
        }

        public Task<Church> FindById(Guid id)
        {
            return _context.Churches.Where(c => c.Id == id)
                .FirstOrDefaultAsync();
        }

        public Church Add(Church entity)
        {
            return _context.Churches.Add(entity)
                .Entity;
        }

        public IQueryable<Church> FindAll()
        {
            throw new NotImplementedException();
        }

        public IQueryable<Church> FindBy(Expression<Func<Church, bool>> expression)
        {
            throw new NotImplementedException();
        }

        public Task<Church> UpdateAsync(Church entity)
        {
            throw new NotImplementedException();
        }

        public Task<Church> DeleteAsync(Church entity)
        {
            throw new NotImplementedException();
        }
    }
}